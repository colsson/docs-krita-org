msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-02 12:00\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___filters___blur.pot\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/filters/Lens-blur-filter.png"
msgstr ""

#: ../../reference_manual/filters/blur.rst:1
msgid "Overview of the blur filters."
msgstr ""

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:15
#: ../../reference_manual/filters/blur.rst:39
msgid "Blur"
msgstr "模糊"

#: ../../reference_manual/filters/blur.rst:10
#: ../../reference_manual/filters/blur.rst:25
msgid "Gaussian Blur"
msgstr "高斯模糊"

#: ../../reference_manual/filters/blur.rst:10
msgid "Filters"
msgstr ""

#: ../../reference_manual/filters/blur.rst:17
msgid ""
"The blur filters are used to smoothen out the hard edges and details in the "
"images. The resulting image is blurry. below is an example of a blurred "
"image. The image of Kiki on right is the result of blur filter applied to "
"the image on left."
msgstr ""

#: ../../reference_manual/filters/blur.rst:21
msgid ".. image:: images/filters/Blur.png"
msgstr ""

#: ../../reference_manual/filters/blur.rst:22
msgid "There are many different filters for blurring:"
msgstr ""

#: ../../reference_manual/filters/blur.rst:27
msgid ""
"You can input the horizontal and vertical radius for the amount of blurring "
"here."
msgstr ""

#: ../../reference_manual/filters/blur.rst:30
msgid ".. image:: images/filters/Gaussian-blur.png"
msgstr ""

#: ../../reference_manual/filters/blur.rst:32
msgid "Motion Blur"
msgstr "运动模糊"

#: ../../reference_manual/filters/blur.rst:34
msgid ""
"Doesn't only blur, but also subtly smudge an image into a direction of the "
"specified angle thus giving a feel of motion to the image. This filter is "
"often used to create effects of fast moving objects."
msgstr ""

#: ../../reference_manual/filters/blur.rst:37
msgid ".. image:: images/filters/Motion-blur.png"
msgstr ""

#: ../../reference_manual/filters/blur.rst:41
msgid "This filter creates a regular blur."
msgstr ""

#: ../../reference_manual/filters/blur.rst:44
msgid ".. image:: images/filters/Blur-filter.png"
msgstr ""

#: ../../reference_manual/filters/blur.rst:46
msgid "Lens Blur"
msgstr "镜头模糊"

#: ../../reference_manual/filters/blur.rst:48
msgid "Lens Blur Algorithm."
msgstr ""
