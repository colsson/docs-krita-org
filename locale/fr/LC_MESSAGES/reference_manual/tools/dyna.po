msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 08:02+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:38
msgid ""
".. image:: images/icons/dyna_tool.svg\n"
"   :alt: tooldyna"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:1
msgid "Krita's dynamic brush tool reference."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:11
msgid "Dyna"
msgstr "Dyna"

#: ../../reference_manual/tools/dyna.rst:16
msgid "Dynamic Brush Tool"
msgstr "Brosse dynamique"

#: ../../reference_manual/tools/dyna.rst:18
msgid "|tooldyna|"
msgstr ""

#: ../../reference_manual/tools/dyna.rst:20
msgid ""
"Add custom smoothing dynamics to your brush. This will give you similar "
"smoothing results as the normal freehand brush. There are a couple options "
"that you can change."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:22
msgid "Mass"
msgstr "Masse"

#: ../../reference_manual/tools/dyna.rst:23
msgid ""
"Average your movement to make it appear smoother. Higher values will make "
"your brush move slower."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:25
msgid "Drag"
msgstr "Faire glisser"

#: ../../reference_manual/tools/dyna.rst:25
msgid ""
"A rubberband effect that will help your lines come back to your cursor. "
"Lower values will make the effect more extreme."
msgstr ""

#: ../../reference_manual/tools/dyna.rst:27
msgid "Recommended values are around 0.02 Mass and 0.92 Drag."
msgstr ""
