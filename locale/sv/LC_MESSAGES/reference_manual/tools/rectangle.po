# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-20 11:33+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Ratio"
msgstr "Förhållande"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:26
msgid ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: toolrectangle"
msgstr ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: Rektangelverktyg"

#: ../../reference_manual/tools/rectangle.rst:1
msgid "Krita's rectangle tool reference."
msgstr "Referens för Kritas rektangelverktyg."

#: ../../reference_manual/tools/rectangle.rst:10
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/rectangle.rst:10
msgid "Rectangle"
msgstr "Rektangel"

#: ../../reference_manual/tools/rectangle.rst:15
msgid "Rectangle Tool"
msgstr "Rektangelverktyg"

#: ../../reference_manual/tools/rectangle.rst:17
msgid "|toolrectangle|"
msgstr "|toolrectangle|"

#: ../../reference_manual/tools/rectangle.rst:19
msgid ""
"This tool can be used to paint rectangles, or create rectangle shapes on a "
"vector layer. Click and hold |mouseleft| to indicate one corner of the "
"rectangle, drag to the opposite corner, and release the button."
msgstr ""
"Verktyget kan användas för att rita rektanglar, eller skapa rektangelformer "
"på ett vektorlager. Klicka och håll nere vänster musknapp för att ange ett "
"av rektangelns hörn, dra till motsatta hörnet, och släpp knappen."

#: ../../reference_manual/tools/rectangle.rst:22
msgid "Hotkeys and Sticky-keys"
msgstr "Snabbtangenter och klistriga tangenter"

#: ../../reference_manual/tools/rectangle.rst:24
msgid "There's no default hotkey for switching to rectangle."
msgstr "Det finns ingen förvald snabbtangent för att byta till rektangel."

#: ../../reference_manual/tools/rectangle.rst:26
msgid ""
"If you hold :kbd:`Shift` while drawing, a square will be drawn instead of a "
"rectangle. Holding :kbd:`Ctrl` will change the way the rectangle is "
"constructed. Normally, the first mouse click indicates one corner and the "
"second click the opposite. With :kbd:`Ctrl`, the initial mouse position "
"indicates the center of the rectangle, and the final mouse position "
"indicates a corner. You can press :kbd:`Alt` while still keeping |mouseleft| "
"down to move the rectangle to a different location."
msgstr ""
"Om man håller nere :kbd:`Shift` när man ritar, ritas en kvadrat istället för "
"en rektangel. Att hålla nere :kbd:`Ctrl` ändrar sättet som rektangeln "
"konstrueras. Normalt anger det första musklicket ett hörn och det andra "
"motsatt hörn. Med :kbd:`Ctrl` anger den ursprungliga muspositionen "
"rektangelns centrum, och den slutliga positionen ett hörn. Man kan hålla "
"nere :kbd:`Alt` medan vänster musknapp fortfarande hålls nere för att flytta "
"rektangeln till en annan plats."

#: ../../reference_manual/tools/rectangle.rst:28
msgid ""
"You can change between the corner/corner and center/corner drawing methods "
"as often as you want by pressing or releasing :kbd:`Ctrl`, provided that you "
"keep |mouseleft| pressed. With :kbd:`Ctrl` pressed, mouse movements will "
"affect all four corners of the rectangle (relative to the center), without :"
"kbd:`Ctrl`, one of the corners is unaffected."
msgstr ""
"Man kan byta mellan ritmetoderna hörn/hörn och centrum/hörn när som helst "
"genom att trycka ner eller släppa :kbd:`Ctrl`, under förutsättning att "
"vänster musknapp hålls nere. Med  :kbd:`Ctrl`nertryckt påverkar musrörelser "
"rektangelns alla fyra hörn (relativt till centrum), utan :kbd:`Ctrl` är ett "
"av hörnen opåverkat."

#: ../../reference_manual/tools/rectangle.rst:32
msgid "Tool Options"
msgstr "Verktygsalternativ"

#: ../../reference_manual/tools/rectangle.rst:35
msgid "Fill"
msgstr "Fyll"

#: ../../reference_manual/tools/rectangle.rst:37
msgid "Not filled"
msgstr "Inte fylld"

#: ../../reference_manual/tools/rectangle.rst:38
msgid "The rectangle will be transparent from the inside."
msgstr "Rektangeln är genomskinlig inuti."

#: ../../reference_manual/tools/rectangle.rst:39
msgid "Foreground color"
msgstr "Förgrundsfärg"

#: ../../reference_manual/tools/rectangle.rst:40
msgid "The rectangle will use the foreground color as fill."
msgstr "Rektangeln fylls i med förgrundsfärgen."

#: ../../reference_manual/tools/rectangle.rst:41
msgid "Background color"
msgstr "Bakgrundsfärg"

#: ../../reference_manual/tools/rectangle.rst:42
msgid "The rectangle will use the background color as fill."
msgstr "Rektangeln fylls i med bakgrundsfärgen."

#: ../../reference_manual/tools/rectangle.rst:44
msgid "Pattern"
msgstr "Mönster"

#: ../../reference_manual/tools/rectangle.rst:44
msgid "The rectangle will use the active pattern as fill."
msgstr "Rektangeln fylls i med det aktiva mönstret."

#: ../../reference_manual/tools/rectangle.rst:47
msgid "Outline"
msgstr "Kontur"

#: ../../reference_manual/tools/rectangle.rst:49
msgid "No Outline"
msgstr "Ingen kontur"

#: ../../reference_manual/tools/rectangle.rst:50
msgid "The Rectangle will render without outline."
msgstr "Rektangeln återges utan kontur."

#: ../../reference_manual/tools/rectangle.rst:52
msgid "Brush"
msgstr "Pensel"

#: ../../reference_manual/tools/rectangle.rst:52
msgid "The Rectangle will use the current selected brush to outline."
msgstr "Rektangeln använder penseln som för närvarande är vald för konturen."

#: ../../reference_manual/tools/rectangle.rst:55
msgid ""
"On vector layers, the rectangle will not render with a brush outline, but "
"rather a vector outline."
msgstr ""
"På vektorlager återges inte rektangeln med en penselkontur, utan istället "
"med en vektorkontur."

#: ../../reference_manual/tools/rectangle.rst:57
msgid "Anti-aliasing"
msgstr "Kantutjämning"

#: ../../reference_manual/tools/rectangle.rst:58
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Ändrar om markeringar ska få vävkanter. Vissa människor föredrar skarpa "
"naggade kanter för sina markeringar."

#: ../../reference_manual/tools/rectangle.rst:59
msgid "Width"
msgstr "Bredd"

#: ../../reference_manual/tools/rectangle.rst:60
msgid ""
"Gives the current width. Use the lock to force the next selection made to "
"this width."
msgstr ""
"Ger den aktuella bredden. Använd lås för att tvinga nästa markering att få "
"den här bredden."

#: ../../reference_manual/tools/rectangle.rst:61
msgid "Height"
msgstr "Höjd"

#: ../../reference_manual/tools/rectangle.rst:62
msgid ""
"Gives the current height. Use the lock to force the next selection made to "
"this height."
msgstr ""
"Ger den aktuella höjden. Använd lås för att tvinga nästa markering att få "
"den här höjden."

#: ../../reference_manual/tools/rectangle.rst:66
msgid ""
"Gives the current ratio. Use the lock to force the next selection made to "
"this ratio."
msgstr ""
"Ger det aktuella förhållandet. Använd lås för att tvinga nästa markering att "
"få det här förhållandet."

#: ../../reference_manual/tools/rectangle.rst:68
msgid "Round X"
msgstr "Avrunda X"

#: ../../reference_manual/tools/rectangle.rst:70
msgid "The horizontal radius of the rectangle corners."
msgstr "Rektangelhörnens horisontella radie."

#: ../../reference_manual/tools/rectangle.rst:72
msgid "Round Y"
msgstr "Avrunda Y"

#: ../../reference_manual/tools/rectangle.rst:74
msgid "The vertical radius of the rectangle corners."
msgstr "Rektangelhörnens vertikala radie."
