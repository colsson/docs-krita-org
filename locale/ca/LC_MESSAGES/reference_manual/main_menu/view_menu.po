# Translation of docs_krita_org_reference_manual___main_menu___view_menu.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 21:11+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<generated>:1
msgid "Show Painting Previews"
msgstr "Mostra les vistes prèvies de la pintura"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/main_menu/view_menu.rst:1
msgid "The view menu in Krita."
msgstr "El menú Visualitza en el Krita."

#: ../../reference_manual/main_menu/view_menu.rst:11
msgid "View"
msgstr "Visualitza"

#: ../../reference_manual/main_menu/view_menu.rst:11
msgid "Wrap around mode"
msgstr "Mode d'ajust cíclic"

#: ../../reference_manual/main_menu/view_menu.rst:16
msgid "View Menu"
msgstr "El menú Visualitza"

#: ../../reference_manual/main_menu/view_menu.rst:18
msgid "Show Canvas Only"
msgstr "Mostra només el llenç"

#: ../../reference_manual/main_menu/view_menu.rst:19
msgid ""
"Only shows the canvas and what you have configured to show in :guilabel:"
"`Canvas Only` settings."
msgstr ""
"Només mostra el llenç i el que heu configurat per a mostrar als ajustaments "
"de :guilabel:`Només el llenç`."

#: ../../reference_manual/main_menu/view_menu.rst:20
msgid "Fullscreen mode"
msgstr "Mode de pantalla completa"

#: ../../reference_manual/main_menu/view_menu.rst:21
msgid "This will hide the system bar."
msgstr "Ocultarà la barra del sistema."

#: ../../reference_manual/main_menu/view_menu.rst:22
msgid "Wrap Around Mode"
msgstr "Mode d'ajust cíclic"

#: ../../reference_manual/main_menu/view_menu.rst:23
msgid ""
"This will show the image as if tiled orthographically. Very useful for "
"tiling 3d textures. Hit :kbd:`W` to quickly activate it."
msgstr ""
"Mostrarà la imatge com si estigués ortogràficament en mosaic. Molt útil per "
"posar en mosaic les textures en 3D. Premeu la tecla :kbd:`W` per activar-la "
"amb rapidesa."

#: ../../reference_manual/main_menu/view_menu.rst:24
msgid "Instant Preview"
msgstr "Vista prèvia instantània"

#: ../../reference_manual/main_menu/view_menu.rst:25
msgid "Toggle :ref:`instant_preview` globally."
msgstr "Alterna la :ref:`instant_preview` globalment."

#: ../../reference_manual/main_menu/view_menu.rst:26
msgid "Soft Proofing"
msgstr "Provatura suau"

#: ../../reference_manual/main_menu/view_menu.rst:27
msgid "Activate :ref:`soft_proofing`."
msgstr "Activa la :ref:`soft_proofing`."

#: ../../reference_manual/main_menu/view_menu.rst:28
msgid "Out of Gamut Warnings"
msgstr "Avisos de fora de la gamma"

#: ../../reference_manual/main_menu/view_menu.rst:29
msgid "See the :ref:`soft_proofing` page for details."
msgstr ""
"Vegeu la pàgina :ref:`soft_proofing` del manual d'usuari per obtenir més "
"informació."

#: ../../reference_manual/main_menu/view_menu.rst:30
msgid "Canvas"
msgstr "Llenç"

#: ../../reference_manual/main_menu/view_menu.rst:31
msgid "Contains view manipulation actions."
msgstr "Conté les accions per a la manipulació de la vista."

#: ../../reference_manual/main_menu/view_menu.rst:32
msgid "Mirror View"
msgstr "Visualització en mirall"

#: ../../reference_manual/main_menu/view_menu.rst:33
msgid ""
"This will mirror the view. Hit :kbd:`M` to quickly activate it. Very useful "
"during painting."
msgstr ""
"Això reflectirà la vista. Premeu la tecla :kbd:`M` per activar-la amb "
"rapidesa. Molt útil durant la pintura."

#: ../../reference_manual/main_menu/view_menu.rst:34
msgid "Show Rulers"
msgstr "Mostra els regles"

#: ../../reference_manual/main_menu/view_menu.rst:35
msgid ""
"This will display a set of rulers. |mouseright| the rulers after showing "
"them, to change the units."
msgstr ""
"Això mostrarà un conjunt de regles. Fer |mouseright| sobre els regles per a "
"canviar les unitats."

#: ../../reference_manual/main_menu/view_menu.rst:36
msgid "Rulers track pointer"
msgstr "Els regles segueixen el punter"

#: ../../reference_manual/main_menu/view_menu.rst:37
msgid ""
"This adds a little marker to the ruler to show where the mouse is in "
"relation to them."
msgstr ""
"Afegirà un petit marcador al regle per a mostrar on es troba el ratolí en "
"relació seu."

#: ../../reference_manual/main_menu/view_menu.rst:38
msgid "Show Guides"
msgstr "Mostra les guies"

#: ../../reference_manual/main_menu/view_menu.rst:39
msgid "Show or hide the guides."
msgstr "Mostra o oculta les guies."

#: ../../reference_manual/main_menu/view_menu.rst:40
msgid "Lock Guides"
msgstr "Bloqueja les guies"

#: ../../reference_manual/main_menu/view_menu.rst:41
msgid "Prevent the guides from being able to be moved by the cursor."
msgstr "Evita que les guies es puguin moure amb el cursor."

#: ../../reference_manual/main_menu/view_menu.rst:42
msgid "Show Status Bar"
msgstr "Mostra la barra d'estat"

#: ../../reference_manual/main_menu/view_menu.rst:43
msgid ""
"This will show the status bar. The status bar contains a lot of important "
"information, a zoom widget, and the button to switch Selection Display Mode."
msgstr ""
"Mostrarà la barra d'estat. Aquesta conté molta informació important, un "
"estri de zoom i el botó per alternar el mode visualització de la selecció."

#: ../../reference_manual/main_menu/view_menu.rst:44
msgid "Show Grid"
msgstr "Mostra la quadrícula"

# skip-rule: t-apo_ini
#: ../../reference_manual/main_menu/view_menu.rst:45
msgid "Shows and hides the grid. :kbd:`Ctrl + Shift + '`"
msgstr "Mostra o oculta la quadrícula. :kbd:`Ctrl + Majús. + '`"

#: ../../reference_manual/main_menu/view_menu.rst:46
msgid "Show Pixel Grid"
msgstr "Mostra la quadrícula de píxels"

#: ../../reference_manual/main_menu/view_menu.rst:47
msgid "Show the pixel grid as configured in the :ref:`display_settings`."
msgstr ""
"Mostra la quadrícula de píxels segons el configurat als :ref:"
"`display_settings`."

#: ../../reference_manual/main_menu/view_menu.rst:48
msgid "Snapping"
msgstr "Ajusta"

#: ../../reference_manual/main_menu/view_menu.rst:49
msgid "Toggle the :ref:`snapping` types."
msgstr "Commuta els tipus d':ref:`snapping`."

#: ../../reference_manual/main_menu/view_menu.rst:50
msgid "Show Painting Assistants"
msgstr "Mostra els assistents de pintura"

#: ../../reference_manual/main_menu/view_menu.rst:51
msgid "Shows or hides the Assistants."
msgstr "Mostra o oculta els assistents."

#: ../../reference_manual/main_menu/view_menu.rst:53
msgid "Shows or hides the Previews."
msgstr "Mostra o oculta les vistes prèvies."
