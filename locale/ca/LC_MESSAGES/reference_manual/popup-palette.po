# Translation of docs_krita_org_reference_manual___popup-palette.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 21:03+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/popup-palette.rst:1
msgid "The Pop-up Palette in Krita"
msgstr "La paleta emergent al Krita"

#: ../../reference_manual/popup-palette.rst:10
#: ../../reference_manual/popup-palette.rst:14
msgid "Pop-up Palette"
msgstr "Paleta emergent"

#: ../../reference_manual/popup-palette.rst:16
msgid ""
"The Pop-up Palette is a feature unique to Krita amongst the digital painting "
"applications. It is designed to increase productivity and save time of the "
"artists by providing quick access to some of the most frequently used tools "
"and features in Krita. The Pop-up palette can be accessed by |mouseright| on "
"the canvas. A circular palette similar to what is shown in the image below "
"will spawn at the position your mouse cursor."
msgstr ""
"La paleta emergent és una característica única al Krita entre les "
"aplicacions de pintura digital. Està dissenyada per a fer augmentar la "
"productivitat i estalviar temps als artistes en proporcionar un accés ràpid "
"a algunes de les eines i funcions més utilitzades en el Krita. S'hi pot "
"accedir fent |mouseright| sobre el llenç. Es generarà una paleta circular "
"similar a la que es mostra a la imatge de sota a la posició del cursor del "
"ratolí."

#: ../../reference_manual/popup-palette.rst:19
msgid ".. image:: images/popup-palette-detail.svg"
msgstr ".. image:: images/popup-palette-detail.svg"

#: ../../reference_manual/popup-palette.rst:20
msgid ""
"As shown in the image above, the pop-up palette has the following tools and "
"quick access shortcuts integrated into it"
msgstr ""
"Com es mostra a la imatge de dalt, la paleta emergent conté integrades les "
"següents eines i dreceres d'accés ràpid."

#: ../../reference_manual/popup-palette.rst:22
msgid ""
"Foreground color and Background color indicators on the top left of the "
"palette."
msgstr ""
"Els indicadors del color de primer pla i de fons a la part superior dreta de "
"la paleta."

#: ../../reference_manual/popup-palette.rst:23
msgid ""
"A canvas rotation circular slider, which can help the artist quickly rotate "
"the canvas while painting."
msgstr ""
"Un control lliscant circular per al gir del llenç, el qual pot ajudar a "
"l'artista a girar ràpidament el llenç mentre pinta."

#: ../../reference_manual/popup-palette.rst:24
msgid ""
"A group of brush presets, based on the tag selected by the artist. By "
"default the **My Favorite** tag is selected. By default only first 10 "
"presets from the tag are shown, however you can change the number of brush "
"presets shown by changing the value in the :ref:`Miscellaneous Settings "
"Section <misc_settings>` of the dialog box."
msgstr ""
"Un grup de pinzells predefinits, basat en l'etiqueta seleccionada per "
"l'artista. De manera predeterminada està seleccionada l'etiqueta **My "
"Favorite** (**Els meus preferits**). De manera predeterminada només es "
"mostraran els primers 10 predefinits de l'etiqueta, no obstant això, podreu "
"canviar el nombre de pinzells predefinits que es mostraran canviant el valor "
"a la :ref:`secció ajustaments de Miscel·lània <misc_settings>` del diàleg."

#: ../../reference_manual/popup-palette.rst:25
msgid ""
"Color Selector with which you can select the hue from the circular ring and "
"lightness and saturation from the triangular area in the middle."
msgstr ""
"El selector de color amb el qual podreu seleccionar el to des de l'anell "
"circular, i la claredat i saturació des de l'àrea triangular que hi ha al "
"centre."

#: ../../reference_manual/popup-palette.rst:26
msgid ""
"Color history area shows the most recent color swatches that you have used "
"while painting."
msgstr ""
"L'àrea de l'historial de color mostra les mostres de color més recents que "
"heu utilitzat en pintar."

#: ../../reference_manual/popup-palette.rst:27
msgid ""
"The tag list for brush preset will show you the list of both custom and "
"default tags to choose from, selecting a tag from this list will show the "
"corresponding brush presets in the palette."
msgstr ""
"La llista d'etiquetes per al pinzell predefinit us mostrarà tant les "
"etiquetes personalitzades com les predeterminades per a que trieu, en "
"seleccionar una etiqueta d'aquesta llista, es mostraran a la paleta els "
"pinzells predefinits corresponents."

#: ../../reference_manual/popup-palette.rst:28
msgid ""
"The common brush options such as size, opacity, angle et cetera will be "
"shown when you click the **>** icon. A dialog box will appear which will "
"have the sliders to adjust the brush options. You can choose which options "
"are shown in this dialog box by clicking on the settings icon."
msgstr ""
"Les opcions de pinzell habituals com la mida, opacitat, angle, etc. es "
"mostraran quan feu clic a la icona «>». Apareixerà un diàleg que tindrà "
"controls lliscants per ajustar les opcions del pinzell. Podreu triar quines "
"opcions es mostraran en aquest diàleg fent clic a la icona ajustaments."

#: ../../reference_manual/popup-palette.rst:29
msgid "The zoom slider allows you to quickly zoom the canvas."
msgstr "El control lliscant de zoom permet fer zoom ràpidament sobre el llenç."

#: ../../reference_manual/popup-palette.rst:30
msgid "The 100% button sets the zoom to the 100% of the image size."
msgstr "El botó al 100% establirà el zoom al 100% de la mida de la imatge."

#: ../../reference_manual/popup-palette.rst:31
msgid ""
"The button with the canvas icon switches to the canvas only mode, where the "
"toolbar and dockers are hidden."
msgstr ""
"El botó amb la icona del llenç alternarà al mode de només el llenç, on la "
"barra d'eines i els acobladors estaran ocults."

#: ../../reference_manual/popup-palette.rst:32
msgid ""
"The button with the mirror icon mirrors the canvas to help you spot the "
"errors in the painting."
msgstr ""
"El botó amb la icona de mirall reflecteix el llenç per ajudar-vos a detectar "
"els errors en la pintura."
