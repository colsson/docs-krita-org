# Translation of docs_krita_org_reference_manual___filters___wavelet_decompose.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___filters___wavelet_decompose\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:35+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/filters/wavelet_decompose.rst:None
msgid ".. image:: images/filters/Wavelet_decompose.png"
msgstr ".. image:: images/filters/Wavelet_decompose.png"

#: ../../reference_manual/filters/wavelet_decompose.rst:1
msgid "Overview of the wavelet decompose in Krita."
msgstr "Огляд засобу розкладу на вейвлети у Krita."

#: ../../reference_manual/filters/wavelet_decompose.rst:11
#: ../../reference_manual/filters/wavelet_decompose.rst:16
msgid "Wavelet Decompose"
msgstr "Розклад на вейвлети"

#: ../../reference_manual/filters/wavelet_decompose.rst:18
msgid ""
"Wavelet decompose uses wavelet scales to turn the current layer into a set "
"of layers with each holding a different type of pattern that is visible "
"within the image. This is used in texture and pattern making to remove "
"unwanted noise quickly from a texture."
msgstr ""
"У розкладі на вейвлети використовується масштабування вейвлетів для "
"перетворення поточного шару на набір шарів, у якому кожен шар містить інший "
"тип видимого на зображенні візерунка. Цим можна скористатися у текстурі і "
"візерунку для швидкого вилучення небажаного шуму з текстури."

#: ../../reference_manual/filters/wavelet_decompose.rst:20
msgid "You can find it under :menuselection:`Layers`."
msgstr "Знайти відповідні пункти можна у меню :menuselection:`Шари`."

#: ../../reference_manual/filters/wavelet_decompose.rst:22
msgid ""
"When you select it, it will ask for the amount of wavelet scales. More "
"scales, more different layers. Press :guilabel:`OK`, and it will generate a "
"group layer containing the layers with their proper blending modes:"
msgstr ""
"Після вибору інструмента програма попросить вас вказати кількість шкал "
"вейвлетів. Чим більше шкал, тим більше буде різних шарів. Натисніть кнопку :"
"guilabel:`Гаразд` і програма створить груповий шар, у якому містяться шари "
"із відповідними режимами змішування:"

#: ../../reference_manual/filters/wavelet_decompose.rst:27
msgid ""
"Adjust a given layer with middle gray to neutralize it, and merge everything "
"with the :guilabel:`Grain Merge` blending mode to merge it into the end "
"image properly."
msgstr ""
"Скоригуйте заданий шар середнім сірим для його нейтралізації і об'єднайте "
"усе з використанням режиму :guilabel:`Об’єднання зерен` для створення "
"кінцевого зображення."
