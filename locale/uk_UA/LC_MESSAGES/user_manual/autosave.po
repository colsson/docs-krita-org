# Translation of docs_krita_org_user_manual___autosave.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___autosave\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-08 03:36+0200\n"
"PO-Revision-Date: 2019-05-08 07:15+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/save_incremental_version.png"
msgstr ".. image:: images/save_incremental_version.png"

#: ../../user_manual/autosave.rst:1
msgid "How AutoSave and Backup Files Work in Krita"
msgstr "Як працюють автоматичне збереження та файли резервних копій у Krita"

#: ../../user_manual/autosave.rst:10 ../../user_manual/autosave.rst:20
msgid "Saving"
msgstr "Збереження"

#: ../../user_manual/autosave.rst:10
msgid "Autosave"
msgstr "Автозбереження"

#: ../../user_manual/autosave.rst:10
msgid "Backup"
msgstr "Резервне копіювання"

#: ../../user_manual/autosave.rst:15
msgid "Saving, AutoSave and Backup Files"
msgstr "Збереження, автоматичне збереження та файли резервних копій"

#: ../../user_manual/autosave.rst:17
msgid ""
"Krita does its best to keep your work safe. But if you want to make sure "
"that you won't lose work, you will need to understand how Saving, AutoSave "
"and Backup Files work in Krita."
msgstr ""
"Krita робить усе, щоб зберегти усі результати вашої роботи. Щоб не втратити "
"створеного, вам слід чітко розуміти, яка працює збереження, автоматичне "
"збереження та резервне копіювання файлів у Krita."

#: ../../user_manual/autosave.rst:22
msgid ""
"Krita does not store your images somewhere without your intervention. You "
"need to save your work, or it will be lost, irretrievably. Krita can save "
"your images in many formats. You should always save your work in Krita's "
"native format, ``.kra`` because that supports all Krita's features."
msgstr ""
"Krita не зберігає ваші зображення десь на диску без вашої згоди. Вам слід "
"зберегти результати вашої роботи, інакше їх буде безповоротно втрачено. "
"Krita може зберігати ваші зображення у багатьох форматах. Також вам слід "
"завжди зберігати вашу роботу у власному форматі Krita, ``.kra``, оскільки у "
"цьому форматі передбачено підтримку усіх можливостей Krita."

#: ../../user_manual/autosave.rst:25
msgid ""
"Additionally, you can export your work to other formats, for compatibility "
"with other applications or publication on the Web or on paper. Krita will "
"warn which aspects of your work are going to be lost when you save to "
"another format than ``.kra`` and offers to make a ``.kra`` file for you as "
"well."
msgstr ""
"Крім того, ви можете експортувати вашу роботу у інших форматах для "
"сумісності із іншими програмами, оприлюднення в інтернеті або друку на "
"папері. Krita попереджатиме вас про ті елементи вашої роботи, які буде "
"втрачено при збереженні даних у форматах, відмінних від ``.kra``, і "
"пропонуватиме вам зберегти дані і у форматі ``.kra``."

#: ../../user_manual/autosave.rst:27
msgid ""
"If you save your work, Krita will ask you where it should save on your "
"computer. By default, this is the Pictures folder in your User folder: this "
"is true for all operating systems."
msgstr ""
"Під час збереження вашої роботи Krita попросить вас вказати, де саме на "
"комп'ютері слід зберегти файл з даними. Типово, це тека малюнків у вашій "
"теці користувача — це правило справджується у всіх операційних системах."

#: ../../user_manual/autosave.rst:29
msgid ""
"If you use \"Save As\" your image will be saved under a new name. The "
"original file under its own name will not be deleted. From now on, your file "
"will be saved under the new name."
msgstr ""
"Якщо ви скористаєтеся пунктом :guilabel:`Зберегти як`, ваше зображення буде "
"збережено із іншою назвою. Початковий файл із початковою назвою при цьому "
"вилучено не буде. З моменту збереження програма працюватиме із файлом із "
"новою назвою."

#: ../../user_manual/autosave.rst:31
msgid ""
"If you use \"Export\" using a new filename, a new file will be created with "
"a new name. The file you have open will keep the new name, and the next time "
"you save it, it will be saved under the old name."
msgstr ""
"Якщо ви скористаєтеся пунктом :guilabel:`Експортувати`, скориставшись новою "
"назвою, буде створено новий файл із новою назвою. Відкритий вами спочатку "
"файл збереже назву. Наступного разу, коли ви скористаєтеся можливістю його "
"збереження, дані буде збережено до файла зі старою назвою."

#: ../../user_manual/autosave.rst:33
msgid "You can Save, Save As and Export to any file format."
msgstr ""
"Ви можете зберігати, зберігати із новою назвою та експортувати дані до файла "
"у будь-якому форматі."

#: ../../user_manual/autosave.rst:36
msgid "See also"
msgstr "Див. також"

#: ../../user_manual/autosave.rst:39
msgid ":ref:`Saving for the Web <saving_for_the_web>`"
msgstr ":ref:`Збереження даних для інтернету <saving_for_the_web>`"

#: ../../user_manual/autosave.rst:43
msgid "AutoSave"
msgstr "Автозбереження"

#: ../../user_manual/autosave.rst:45
msgid ""
"AutoSave is what happens when you've worked for a bit and not saved your "
"work yourself: Krita will save your work for you. Autosave files are by "
"default hidden in your file manager. You can configure Krita 4.2 and up to "
"create autosave files that are visible in your file manager. By default, "
"Krita autosaves every fifteen minutes; you can configure that in the File "
"tab of the General Settings page of the Configure Krita dialog, which is in "
"the Settings menu (Linux, Windows) or in the Application menu (macOS)."
msgstr ""
"Автоматичне зберігання даних виконується, якщо ви працюєте над певним "
"зображенням певний проміжок часу, але не зберегли зображення самі — Krita "
"зберігатиме зображення за вас. Файли автоматично збережених копій будуть, "
"типово, невидимими у програмі для керування файлами вашої системи. Ви можете "
"налаштувати Krita 4.2 і новіші версії так, щоб вони створювали файли "
"автоматично збережених копій, які будуть видимими у вашій програмі для "
"керування даними. Типово, Krita автоматично зберігає копії файлів кожні "
"п'ятнадцять хвилин. Налаштувати автоматичне зберігання можна за допомогою "
"вкладки :guilabel:`Файл` сторінки загальних параметрів діалогового вікна "
"налаштовування Krita. Відкрити це вікно можна за допомогою меню :guilabel:"
"`Параметри` (Linux, Windows) або меню :guilabel:`Програми` (macOS)."

#: ../../user_manual/autosave.rst:47
msgid ""
"If you close Krita without saving, your unsaved work is lost and cannot be "
"retrieved. Closing Krita normally also means that autosave files are removed."
msgstr ""
"Якщо ви закриєте вікно Krita без збереження результатів роботи, вашу роботу "
"буде безповоротно втрачено. Закриття вікна Krita у звичайному режимі також "
"означає, що буде вилучено усі файли автоматично збережених копій."

#: ../../user_manual/autosave.rst:50 ../../user_manual/autosave.rst:115
msgid ".. image:: images/file_config_page.png"
msgstr ".. image:: images/file_config_page.png"

#: ../../user_manual/autosave.rst:51
msgid "There are two possibilities:"
msgstr "Існує два варіанти:"

#: ../../user_manual/autosave.rst:53
msgid "You hadn't saved your work at all"
msgstr "Ви взагалі не зберігали результати вашої роботи."

#: ../../user_manual/autosave.rst:54
msgid "You had saved your work already"
msgstr "Ви вже зберегли вашу роботу."

#: ../../user_manual/autosave.rst:57
msgid "AutoSave for Unsaved Files"
msgstr "Автоматичне збереження файлів, які ще не було збережено"

#: ../../user_manual/autosave.rst:59
msgid ""
"If you had not yet saved your work, Krita will create an unnamed AutoSave "
"file."
msgstr ""
"Якщо ви ще не зберігали результати вашої роботи, Krita створить безіменний "
"файл автоматичного збереження."

#: ../../user_manual/autosave.rst:61
msgid ""
"If you're using Linux or macOS, the AutoSave file will be a hidden file in "
"your home directory. If you're using Windows, the AutoSave file will be a "
"file in your user's ``%TEMP%`` folder. In Krita 4.2 and up, you can "
"configure Krita to make the AutoSave files visible by default."
msgstr ""
"Якщо ви користуєтеся Linux або macOS, файл автоматично збереженої копії буде "
"прихованим файлом у вашому домашньому каталозі. Якщо ви користуєтеся "
"Windows, файл автоматично збереженої копії зберігатиметься у теці ``%TEMP%`` "
"вашого користувача. У Krita 4.2 і новіших версіях ви можете налаштувати "
"Krita так, щоб файли автоматично збережених копій типово були видимими."

#: ../../user_manual/autosave.rst:63
msgid ""
"A hidden autosave file will be named like ``.krita-12549-document_1-autosave."
"kra``"
msgstr ""
"Прихований файла автоматично збереженої копії матиме назву, яка подібна до "
"такої: ``.krita-12549-document_1-autosave.kra``"

#: ../../user_manual/autosave.rst:65
msgid ""
"If Krita crashes before you had saved your file, then the next time you "
"start Krita, you will see the file in a dialog that shows up as soon as "
"Krita starts. You can select to restore the files, or to delete them."
msgstr ""
"Якщо Krita аварійно завершить роботу до того, як ви встигнете зберегти ваш "
"файл, під час наступного запуску Krita ви побачите пункти незбережених "
"файлів у початковому діалоговому вікні Krita. Програма запропонує вам або "
"відновити файли з автоматично збережених копій, або вилучити автоматично "
"збережені копії."

#: ../../user_manual/autosave.rst:68
msgid ".. image:: images/autosave_unnamed_restore.png"
msgstr ".. image:: images/autosave_unnamed_restore.png"

#: ../../user_manual/autosave.rst:69
msgid ""
"If Krita crashed, and you're on Windows and your ``%TEMP%`` folder gets "
"cleared, you will have lost your work. Windows does not clear the ``%TEMP%`` "
"folder by default, but you can enable this feature in Settings. Applications "
"like Disk Cleanup or cCleaner will also clear the ``%TEMP%`` folder. Again, "
"if Krita crashes, and you haven't saved your work, and you have something "
"enabled that clear your ``%TEMP%`` folder, you will have lost your work."
msgstr ""
"Якщо Krita завершить роботу в аварійному режимі у Windows, а теку ``%TEMP%`` "
"між запусками програми буде очищено, ви втратите результати своєї роботи. "
"Типово, Windows не спорожнює теку ``%TEMP%`` автоматично, але цю теку може "
"бути спорожнено, якщо відповідну можливість увімкнено у параметрах системи. "
"Крім того, теку ``%TEMP%`` можуть спорожнити програми, подібні до Disk "
"Cleanup або CCleaner. Знову ж таки, якщо Krita завершила роботу в аварійному "
"режимі, ви не встигли зберегти результати вашої роботи і якась із програм "
"спорожнила вашу теку ``%TEMP%``, ви втратили результати вашої роботи."

#: ../../user_manual/autosave.rst:71
msgid ""
"If Krita doesn't crash, and you close Krita without saving your work, Krita "
"will remove the AutoSave file: your work will be gone and cannot be "
"retrieved."
msgstr ""
"Якщо роботу Krita не було завершено в аварійному режимі, і ви закрили вікно "
"Krita без збереження ваших даних, Krita вилучить файл автоматично збереженої "
"копії: вашу роботу буде втрачено, її результати не можна буде відновити."

#: ../../user_manual/autosave.rst:73
msgid ""
"If you save your work and continue, or close Krita and do save your work, "
"the AutoSave file will be removed."
msgstr ""
"Якщо ви збережете результати роботи і продовжите малювати або закриєте вікно "
"Krita зі збереженням результатів роботи, файл автоматично збереженої копії "
"буде вилучено."

#: ../../user_manual/autosave.rst:76
msgid "AutoSave for Saved Files"
msgstr "Автоматичне збереження файлів, які вже було збережено"

#: ../../user_manual/autosave.rst:78
msgid ""
"If you had already saved your work, Krita will create a named AutoSave file."
msgstr ""
"Якщо ви вже зберегли вашу роботу, Krita створить іменований файл автоматично "
"збереженої копії."

#: ../../user_manual/autosave.rst:80
msgid ""
"A hidden named autosave file will look like ``.myimage.kra-autosave.kra``."
msgstr ""
"Назва прихованого файла автоматично збереженої копії виглядатиме так: ``."
"myimage.kra-autosave.kra``."

#: ../../user_manual/autosave.rst:82
msgid ""
"By default, named AutoSave files are hidden. Named AutoSave files are placed "
"in the same folder as the file you were working on."
msgstr ""
"Типово, іменовані файли автоматично збережених копій буде приховано. "
"Іменовані файли автоматично збережених копій зберігатимуться до тієї самої "
"теки, де зберігається файл, над яким ви працюєте."

#: ../../user_manual/autosave.rst:84
msgid ""
"If you start Krita again after it crashed and try to open your original "
"file, Krita will ask you whether to open the AutoSave file instead:"
msgstr ""
"Якщо ви запустите Krita після аварійного завершення роботи і спробуєте "
"відкрити початковий файл, Krita спитає вас, чи не слід замість нього "
"відкрити файл автоматично збереженої копії:"

#: ../../user_manual/autosave.rst:87
msgid ".. image:: images/autosave_named_restore.png"
msgstr ".. image:: images/autosave_named_restore.png"

#: ../../user_manual/autosave.rst:88
msgid ""
"If you choose \"no\", the AutoSave file will be removed. The work that has "
"been done since the last time you saved your file yourself will be lost and "
"cannot be retrieved."
msgstr ""
"Якщо ви виберете варіант :guilabel:`Ні`, файл автоматично збереженої копії "
"буде вилучено. Результати роботи, яку було виконано з часу, коли ви самі "
"останній раз зберігали ваш файл, буде незворотно втрачено."

#: ../../user_manual/autosave.rst:90
msgid ""
"If you choose \"yes\", the AutoSave file will be opened, then removed. The "
"file you have open will have the name of your original file. The file will "
"be set to Modified, so the next time you try to close Krita, Krita will ask "
"you whether you want to save the file. If you choose No, your work is "
"irretrievably gone. It cannot be restored."
msgstr ""
"Якщо ви виберете варіант :guilabel:`Так`, буде відкрито файл автоматично "
"збереженої копії, який після цього буде вилучено. Відкритому вами файлу буде "
"надано назву початкового файла. Стан файла буде встановлено у значення "
"«змінено», отже, якщо ви спробуєте закрити вікно Krita, Krita спитає вас, чи "
"не хочете ви зберегти файл. Якщо ви виберете варіант :guilabel:`Ні`, "
"результати вашої роботи буде незворотно втрачено. Ви не зможете їх відновити."

#: ../../user_manual/autosave.rst:92
msgid ""
"If you use \"Save As\" your image will be saved under a new name. The "
"original file under its own name and its AutoSave file are not deleted. From "
"now on, your file will be saved under the new name; if you save again, an "
"AutoSave file will be created using the new filename."
msgstr ""
"Якщо ви скористаєтеся пунктом :guilabel:`Зберегти як`, ваше зображення буде "
"збережено із іншою назвою, а його файл автоматично збереженої копії не буде "
"вилучено. Початковий файл із початковою назвою при цьому вилучено не буде. З "
"моменту збереження програма працюватиме із файлом із новою назвою; якщо ви "
"знову збережете його, файл автоматично збереженої копії буде створено вже "
"для файла із новою назвою."

#: ../../user_manual/autosave.rst:94
msgid ""
"If you use \"Export\" using a new filename, a new file will be created with "
"a new name. The file you have open will keep the new name, and the next time "
"you save it, the AutoSave file will be created from the last file saved with "
"the current name, that is, not the name you choose for \"Export\"."
msgstr ""
"Якщо ви скористаєтеся пунктом :guilabel:`Експортувати`, скориставшись новою "
"назвою, буде створено новий файл із новою назвою. Відкритий вами спочатку "
"файл збереже назву. Наступного разу, коли ви скористаєтеся можливістю його "
"збереження, файл автоматично збереженої копії буде створено для останнього "
"файла, який було збережено із поточною назвою, тобто для файла зі старою "
"назвою."

#: ../../user_manual/autosave.rst:98
msgid "Backup Files"
msgstr "Файли резервних копій"

#: ../../user_manual/autosave.rst:100
msgid "There are three kinds of Backup files"
msgstr "Існує три типи файлів резервних копій"

#: ../../user_manual/autosave.rst:102
msgid ""
"Ordinary Backup files that are created when you save a file that has been "
"opened from disk"
msgstr ""
"Звичайні файли резервних копій, які програма створює, коли ви зберігаєте "
"файл, який було відкрито з диска."

#: ../../user_manual/autosave.rst:103
msgid ""
"Incremental Backup files that are copies of the file as it is on disk to a "
"numbered backup, and while your file is saved under the current name"
msgstr ""
"Файли нарощувальних резервних копій, які є копіями файла у тому стані, у "
"якому він перебував на диску на момент створення нумерованої резервної "
"копії. Сам основний файл зберігатиметься із використанням його поточної "
"назви."

#: ../../user_manual/autosave.rst:104
msgid ""
"Incremental Version files that are saves of the file you are working on with "
"a new number, leaving alone the existing files on disk."
msgstr ""
"Файли нарощувальних версій, які є нумерованими копіями файла, над яким ви "
"працюєте. При цьому, зміни не зберігаються до початкового файла."

#: ../../user_manual/autosave.rst:108
msgid "Ordinary Backup Files"
msgstr "Звичайні файли резервних копій"

#: ../../user_manual/autosave.rst:110
msgid ""
"If you have opened a file, made changes, then save it, or save a new file "
"after the first time you've saved it, Krita will save a backup of your file."
msgstr ""
"Якщо ви відкриєте файл, внесете до нього зміни, а потім збережете файл, або "
"збережете новий файл, після першого збереження Krita створити резервну копію "
"вашого файла."

#: ../../user_manual/autosave.rst:112
msgid ""
"You can disable this mechanism in the File tab of the General Settings page "
"of the Configure Krita dialog, which is in the Settings menu (Linux, "
"Windows) or in the Application menu (macOS). By default, Backup files are "
"enabled."
msgstr ""
"Ви можете вимкнути цей механізм за допомогою вкладки :guilabel:`Файл` "
"сторінки загальних параметрів діалогового вікна налаштовування Krita. "
"Відкрити це вікно можна за допомогою меню :guilabel:`Параметри` (Linux, "
"Windows) або меню :guilabel:`Програми` (macOS). Типово, створення резервних "
"копій файлів увімкнено."

#: ../../user_manual/autosave.rst:116
msgid ""
"By default, a Backup file will be in the same folder as your original file. "
"You can also choose to save Backup files in the User folder or the ``%TEMP"
"%`` folder; this is not as safe because if you edit two files with the same "
"name in two different folders, their backups will overwrite each other."
msgstr ""
"Типово, файл резервної копії буде створено у тій самій теці, що і ваш "
"початковий файл. Ви також можете наказати програмі зберігати файли резервних "
"копій у теці користувача або теці ``%TEMP%``. Втім, такий спосіб зберігання "
"файлів резервних копій не є безпечним, оскільки, якщо ви редагуєте два файли "
"із однаковими назвами, їхні резервні копії перезаписуватимуть одна одну."

#: ../../user_manual/autosave.rst:118
msgid ""
"By default, a Backup file will have ``~`` as a suffix, to distinguish it "
"from an ordinary file. If you are using Windows, you will have to enable "
"\"show file extensions\" in Windows Explorer to see the extension."
msgstr ""
"Типово, назва файла резервної копії утворюватиметься додаванням суфікса "
"``~``, щоб файл можна було відрізнити від звичайного файла. Якщо ви "
"користуєтеся Windows, вам слід увімкнути показ суфіксів назв файлів у "
"Провіднику Windows, щоб бачити суфікси назв."

#: ../../user_manual/autosave.rst:121
msgid ".. image:: images/file_and_backup_file.png"
msgstr ".. image:: images/file_and_backup_file.png"

#: ../../user_manual/autosave.rst:122
msgid ""
"If you want to open the Backup file, you will have to rename it in your file "
"manager. Make sure the extension ends with ``.kra``."
msgstr ""
"Якщо ви хочете відкрити у програмі файл резервної копії, вам слід "
"перейменувати його у програмі для керування файлами. Останнім суфіксом у "
"назві файла має бути ``.kra``."

#: ../../user_manual/autosave.rst:124
msgid ""
"Every time you save your file, the last version without a ``~`` suffix will "
"be copied to the version with the ``~`` suffix. The contents of the original "
"file will be gone: it will not be possible to restore that version."
msgstr ""
"Кожного разу, коли ви зберігатимете ваш файл, останню версію без суфікса "
"``~`` буде скопійовано до версії із суфіксом ``~``. Вміст початкового файла "
"буде перезаписано — ви не зможете відновити відповідну його версію."

#: ../../user_manual/autosave.rst:127
msgid "Incremental Backup Files"
msgstr "Нарощувальні резервні копії файлів"

#: ../../user_manual/autosave.rst:129
msgid ""
"Incremental Backup files are similar to ordinary Backup files: the last "
"saved state is copied to another file just before saving. However, instead "
"of overwriting the Backup file, the Backup files are numbered:"
msgstr ""
"Файли нарощувальних резервних копій подібні до звичайних файлів резервних "
"копій: останній збережений стан буде скопійовано до іншого файла до "
"збереження. Втім, замість перезаписування файла резервної копії, файли "
"резервних копій просто перенумеровуватимуться:"

#: ../../user_manual/autosave.rst:132
msgid ".. image:: images/save_incremental_backup.png"
msgstr ".. image:: images/save_incremental_backup.png"

#: ../../user_manual/autosave.rst:133
msgid ""
"Use this when you want to keep various known good states of your image "
"throughout your painting process. This takes more disk space, of course."
msgstr ""
"Скористайтеся можливістю нарощувального резервного копіювання, щоб мати "
"послідовність коректних проміжних станів вашого зображення під час процесу "
"малювання. Звичайно ж, такий спосіб зберігання призводитиме до додаткового "
"використання місця на диску."

#: ../../user_manual/autosave.rst:135
msgid ""
"Do not be confused: Krita does not save the current state of your work to "
"the latest Incremental file, but copies the last saved file to the Backup "
"file and then saves your image under the original filename."
msgstr ""
"Не переплутайте: Krita не зберігає поточний стан вашої роботи до останнього "
"файла нарощувальної копії, а копіює останній збережений стан файла до файла "
"резервної копії, а потім зберігає ваше зображення до файла, який ви "
"розпочали редагувати."

#: ../../user_manual/autosave.rst:138
msgid "Incremental Version Files"
msgstr "Нарощувальні файли версій"

#: ../../user_manual/autosave.rst:140
msgid ""
"Incremental Version works a bit like Incremental Backup, but it leaves the "
"original files alone. Instead, it will save a new file with a file number:"
msgstr ""
"Нарощувальні версії працюють подібно до нарощувальних резервних копій, але "
"без внесення змін до початкових файлів. У цьому режимі програма зберігає "
"нову нумеровану версію файла:"
