# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 10:03+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: en FillLayer Krita image images Revoy layers Perline\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: filllayersimplexnoise OpenSimplex\n"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:0
msgid ".. image:: images/layers/fill_layer_simplex_noise.png"
msgstr ".. image:: images/layers/fill_layer_simplex_noise.png"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:1
msgid "How to use fill layers in Krita."
msgstr "Como usar as camadas de preenchimento no Krita."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Layers"
msgstr "Camadas"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Fill"
msgstr "Preenchimento"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Generator"
msgstr "Gerador"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:17
msgid "Fill Layers"
msgstr "Camadas de Preenchimento"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:19
msgid ""
"A Fill Layer is a special layer that Krita generates on-the-fly that can "
"contain either a pattern or a solid color."
msgstr ""
"Uma Camada de Preenchimento é uma camada especial que o Krita gera na altura "
"e que poderá conter um padrão ou uma cor única."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:22
msgid ".. image:: images/layers/Fill_Layer.png"
msgstr ".. image:: images/layers/Fill_Layer.png"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:24
msgid ""
"This fills the layer with a predefined pattern or texture that has been "
"loaded into Krita through the Resource Management interface.  Patterns can "
"be a simple and interesting way to add texture to your drawing or painting, "
"helping to recreate the look of watercolor paper, linen, canvas, hardboard, "
"stone or an infinite other number of options.  For example if you want to "
"take a digital painting and finish it off with the appearance of it being on "
"canvas you can add a Fill Layer with the Canvas texture from the texture "
"pack below and set the opacity very low so the \"threads\" of the pattern "
"are just barley visible.  The effect is quite convincing."
msgstr ""
"Isto preenche a camada com um padrão ou textura predefinido que tenha sido "
"carregado no Krita através da interface de Gestão de Recursos. Os padrões "
"poderão ser uma forma simples e interessante de adicionar texturas ao seu "
"desenho ou pintura, ajudando a recriar a aparência de papel azul, linho, "
"tela, ardósia, pedra ou um número infinito de opções. Por exemplo, se quiser "
"usar uma pintura digital e finalizá-la com a aparência que foi pintada sobre "
"tela, poderá adicionar uma Camada de Preenchimento com a textura de Tela, "
"disponível no pacote de texturas abaixo, e definir a opacidade muito baixa "
"para que os \"fios\" do padrão sejam apenas ligeiramente visíveis. O efeito "
"é bastante convincente."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:26
msgid "Pattern"
msgstr "Padrão"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:26
msgid ""
"You can create your own and use those as well.  For a great set of well "
"designed and useful patterns check out one of our favorite artists and a "
"great friend of Krita, David Revoy's free texture pack (https://www."
"davidrevoy.com/article156/texture-pack-1)."
msgstr ""
"Poderá criar a sua própria ou usar estas da mesma forma. Para um óptimo "
"conjunto de padrões desenhados e úteis, veja um dos nossos artistas "
"favoritos e um grande amigo do Krita, o pacote de texturas gratuitas do "
"David Revoy (https://www.davidrevoy.com/article156/texture-pack-1)."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:29
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:29
msgid ""
"The second option is not quite as exciting, but does the job. Fill the layer "
"with a selected color."
msgstr ""
"A segunda opção não é tão interessante, mas faz o que é necessário. Preenche "
"a camada com uma cor previamente seleccionada."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:36
msgid ""
"A noise generator that isn't Perline Noise (which is what typical 'clouds' "
"generation is), but it looks similar and can actually loop. Uses the "
"OpenSimplex code."
msgstr ""
"Um gerador de ruído que não é do tipo Perline (que corresponde a geração de "
"'nuvens' típicas), mas que tem um aspecto semelhante e que se consegue "
"repetir. Usa o código do OpenSimplex."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:38
msgid "Looping"
msgstr "Em ciclo"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:39
msgid "Whether or not to force the pattern to loop."
msgstr "Se deve ou não forçar o padrão a repetir-se em ciclo."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:40
msgid "Frequency"
msgstr "Frequência"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:41
msgid ""
"The frequency of the waves used to generate the pattern. Higher frequency "
"results in a finer noise pattern."
msgstr ""
"A frequência das ondas usadas para gerar o padrão. Uma maior frequência "
"resulta num padrão de ruído mais fino."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:42
msgid "Ratio"
msgstr "Taxa"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:43
msgid ""
"The ratio of the waves in the x and y dimensions. This makes the noise have "
"a rectangular appearance."
msgstr ""
"A proporção das ondas nas dimensões em X e Y. Isto faz com que o ruído tenha "
"uma aparência rectangular."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid "Simplex Noise"
msgstr "Ruído Simples"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid "Use Custom Seed"
msgstr "Usar um Valor de Base Personalizado"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid ""
"The seed for the random component. You can input any value or text here, and "
"it will always try to use this value to generate the random values with "
"(which then are always the same for a given seed)."
msgstr ""
"O valor de base da componente aleatória. Poderá indicar qualquer valor ou "
"texto aqui, sendo que irá sempre tentar usar este valor para gerar os "
"valores aleatórios (os quais são sempre os mesmos para um dado valor de "
"base)."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:48
msgid "Painting on a fill layer"
msgstr "Pintura sobre uma camada de preenchimento"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:50
msgid ""
"A fill-layer is a single-channel layer, meaning it only has transparency. "
"Therefore, you can erase and paint on fill-layers to make them semi-opaque, "
"or for when you want to have a particular color only. Being single channel, "
"fill-layers are also a little bit less memory-consuming than regular 4-"
"channel paint layers."
msgstr ""
"Uma camada de preenchimento é uma camada com um único canal, o que significa "
"que só tem transparência. Como tal, poderá limpar e pintar nas camadas de "
"preenchimento para as tornar semi-opacas ou quando precisa de ter apenas uma "
"cor em particular. Tendo apenas um único canal, as camadas de preenchimento "
"também têm um consumo de memória mais reduzido que as camadas de pintura "
"normais com os 4 canais."
