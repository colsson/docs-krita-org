# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-18 09:38+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: toolcalligraphy icons demão image calligraphytool\n"
"X-POFile-SpellExtra: images alt guilabel\n"

#: ../../<rst_epilog>:16
msgid ""
".. image:: images/icons/calligraphy_tool.svg\n"
"   :alt: toolcalligraphy"
msgstr ""
".. image:: images/icons/calligraphy_tool.svg\n"
"   :alt: ferramenta de caligrafia"

#: ../../reference_manual/tools/calligraphy.rst:1
msgid "Krita's calligraphy tool reference."
msgstr "A referência à ferramenta de caligrafia."

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Vector"
msgstr "Vector"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Path"
msgstr "Caminho"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Variable Width Stroke"
msgstr "Traço de Espessura Variável"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Calligraphy"
msgstr "Caligrafia"

#: ../../reference_manual/tools/calligraphy.rst:16
msgid "Calligraphy Tool"
msgstr "Ferramenta de Caligrafia"

#: ../../reference_manual/tools/calligraphy.rst:18
msgid "|toolcalligraphy|"
msgstr "|toolcalligraphy|"

#: ../../reference_manual/tools/calligraphy.rst:20
msgid ""
"The Calligraphy tool allows for variable width lines, with input managed by "
"the tablet. Press down with the stylus/left mouse button on the canvas to "
"make a line, lifting the stylus/mouse button ends the stroke."
msgstr ""
"A ferramenta de Caligrafia permite ter linhas de espessura variável, sendo a "
"reacção gerida pela tablete. Carregue com o lápis sobre a área de desenho "
"para desenhar uma linha, enquanto levantando o lápis/botão do rato irá "
"terminar o traço."

#: ../../reference_manual/tools/calligraphy.rst:24
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/calligraphy.rst:26
msgid "**Fill**"
msgstr "**Preencher**"

#: ../../reference_manual/tools/calligraphy.rst:28
msgid "Doesn't actually do anything."
msgstr "De facto, não faz nada."

#: ../../reference_manual/tools/calligraphy.rst:30
msgid "**Calligraphy**"
msgstr "**Caligrafia**"

#: ../../reference_manual/tools/calligraphy.rst:32
msgid ""
"The drop-down menu holds your saved presets, the :guilabel:`Save` button "
"next to it allows you to save presets."
msgstr ""
"A lista contém as suas predefinições gravadas, sendo que o botão para :"
"guilabel:`Gravar` permite-lhe gravar as predefinições."

#: ../../reference_manual/tools/calligraphy.rst:34
msgid "Follow Selected Path"
msgstr "Seguir o Caminho Seleccionado"

#: ../../reference_manual/tools/calligraphy.rst:35
msgid ""
"If a stroke has been selected with the default tool, the calligraphy tool "
"will follow this path."
msgstr ""
"Se tiver seleccionado um traço com a ferramenta predefinida, a ferramenta de "
"caligrafia irá seguir esse caminho."

#: ../../reference_manual/tools/calligraphy.rst:36
msgid "Use Tablet Pressure"
msgstr "Usar a Pressão da Tablete"

#: ../../reference_manual/tools/calligraphy.rst:37
msgid "Uses tablet pressure to control the stroke width."
msgstr "Usa a pressão da tablete para controlar a espessura do traço."

#: ../../reference_manual/tools/calligraphy.rst:38
msgid "Thinning"
msgstr "Afinamento"

#: ../../reference_manual/tools/calligraphy.rst:39
msgid ""
"This allows you to set how much thinner a line becomes when speeding up the "
"stroke. Using a negative value makes it thicker."
msgstr ""
"Isto permite-lhe definir quão fina se torna uma linha à medida que se "
"acelera o traço. Se usar um valor negativo, tornar-se-á mais grossa."

#: ../../reference_manual/tools/calligraphy.rst:40
msgid "Width"
msgstr "Largura"

#: ../../reference_manual/tools/calligraphy.rst:41
msgid "Base width for the stroke."
msgstr "Espessura de base para o traço."

#: ../../reference_manual/tools/calligraphy.rst:42
msgid "Use Tablet Angle"
msgstr "Usar o Ângulo da Tablete"

#: ../../reference_manual/tools/calligraphy.rst:43
msgid ""
"Allows you to use the tablet angle to control the stroke, only works for "
"tablets supporting it."
msgstr ""
"Permite-lhe usar o ângulo da tablete para controlar o traço; só funciona "
"para as tabletes que o suportem."

#: ../../reference_manual/tools/calligraphy.rst:44
msgid "Angle"
msgstr "Ângulo"

#: ../../reference_manual/tools/calligraphy.rst:45
msgid "The angle of the dab."
msgstr "O ângulo da demão."

#: ../../reference_manual/tools/calligraphy.rst:46
msgid "Fixation"
msgstr "Fixação"

#: ../../reference_manual/tools/calligraphy.rst:47
msgid "The ratio of the dab. 1 is thin, 0 is round."
msgstr "A proporção da demão. O valor 1 é fino, enquanto o 0 é redondo."

#: ../../reference_manual/tools/calligraphy.rst:48
msgid "Caps"
msgstr "Extremos"

#: ../../reference_manual/tools/calligraphy.rst:49
msgid "Whether or not an stroke will end with a rounding or flat."
msgstr "Se o traço vai terminar arredondado ou abruptamente."

#: ../../reference_manual/tools/calligraphy.rst:50
msgid "Mass"
msgstr "Massa"

#: ../../reference_manual/tools/calligraphy.rst:51
msgid ""
"How much weight the stroke has. With drag set to 0, high mass increases the "
"'orbit'."
msgstr ""
"Quanto peso tem o traço. Quando o arrastamento for igual a 0, uma massa "
"elevada aumenta a 'órbita'."

#: ../../reference_manual/tools/calligraphy.rst:53
msgid "Drag"
msgstr "Arrastamento"

#: ../../reference_manual/tools/calligraphy.rst:53
msgid ""
"How much the stroke follows the cursor, when set to 0 the stroke will orbit "
"around the cursor path."
msgstr ""
"Quanto é que o traço segue o cursor; se for igual a 0, o traço irá orbitar "
"em torno do caminho do cursor."

#: ../../reference_manual/tools/calligraphy.rst:57
msgid ""
"The calligraphy tool can be edited by the edit-line tool, but currently you "
"can't add or remove nodes without converting it to a normal path."
msgstr ""
"A ferramenta de caligrafia pode ser editada na ferramenta da linha de "
"edição, mas neste momento não consegue adicionar ou remover nós sem a "
"converter para um caminho normal."
